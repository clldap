(in-package :clldap)

;;; was in package toys:
(defmacro do-reader ((var reader &optional result) &body body)
  (let ((read-label (gensym))
        (eof-error (gensym)))
    `(block do-read
       (tagbody
          ,read-label
          (handler-case
              (let ((,var ,reader))
                ,@body)
            (end-of-file (,eof-error)
              (declare (ignore ,eof-error))
              (return-from do-read ,result)))
          (go ,read-label)))))

;;; was in package toys:
(defmacro map-reader (function reader)
  (let ((value (gensym))
        (collector (gensym)))
    `(let (,collector)
       (do-reader (,value ,reader (nreverse ,collector))
         (push (funcall ,function ,value)
               ,collector)))))

;;; records

(defun record-distinguished-name (record)
  "Return the distinguished name (DN) of the record as a string."
  (car record))

(defun record-object-classes (record)
  "Return a list of the object classes of the record in the same format as attributes.
The ATTRIBUTE-* functions will operate successfully on the elements of this list."
  (cadr record))

(defun record-attributes-list (record)
  "Returns a list of the attributes of this record."
  (caddr record))

(defun record-attributes (record attribute-name)
  "Returns a list of the attributes named by ATTRIBUTE-NAME."
  (cdr (assoc attribute-name (record-attributes-list record)
              :test #'string-equal)))

(defun record-attribute (record attribute-name &key (nil-errorp nil))
  "Returns a single attribute named by ATTRIBUTE-NAME.
An error is thrown if RECORD contains multiple values for this attribute.
The argument NIL-ERRORP controls behavior if no attributes match ATTRIBUTE-NAME;
if false, NIL is returned; if true, an error is signalled."
  (let ((attributes (record-attributes record attribute-name)))
    (cond
      ((> (length attributes) 1) (error "Multiple attributes for ~S" attribute-name))
      ((= (length attributes) 1) (car attributes))
      (nil-errorp (error "Attribute ~S not found" attribute-name)))))
      
(defun (setf record-attribute) (new-attribute record attribute-name &key (nil-errorp nil))
  "Sets a single attribute named by ATTRIBUTE-NAME to new-attribute.
An error is thrown if RECORD contains multiple values for this attribute.
The argument NIL-ERRORP controls behavior if no attributes match ATTRIBUTE-NAME;
if false, NIL is returned; if true, an error is signalled."
  (let ((attributes (record-attributes record attribute-name)))
    (cond
      ((> (length attributes) 1) (error "Multiple attributes for ~S" attribute-name))
      ((= (length attributes) 1) (setf (car attributes) new-attribute))
      (nil-errorp (error "Attribute ~S not found" attribute-name)))))
      
(defun record-attribute-value (record attribute &key (nil-errorp nil))
  "Finds a single attribute as by RECORD-ATTRIBUTE, and returns its value.
The attribute's options are returned as a secondary value."
  (with-accessors ((options attribute-options)
		   (value attribute-value))
      (record-attribute record attribute :nil-errorp nil-errorp)
    (values value options)))

(defun (setf record-attribute-value) (new-value record attribute &key (nil-errorp nil))
  "Finds a single attribute as by RECORD-ATTRIBUTE, and sets its value to NEW-VALUE."
  (with-accessors ((options attribute-options)
		   (value attribute-value))
      (record-attribute record attribute :nil-errorp nil-errorp)
    (setf value new-value)))


;;; change records

(defun change-record-change-type (record)
  "Returns the change type of the change record RECORD.
This will be a symbol in the LDIF package."
  (cadr record))


;;; attributes

(defun attribute-options (attribute)
  "Returns a list of strings denoting ATTRIBUTE's options."
  (car attribute))

(defun attribute-value (attribute)
  "Returns the string value of ATTRIBUTE."
  (cadr attribute))

(defun (setf attribute-value) (new-value attribute)
  "Set the string value of ATTRIBUTE to NEW-VALUE."
  (setf (cadr attribute) new-value))
