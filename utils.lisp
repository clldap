(in-package :clldap)

(defun ldap-escape (string)
  (labels ((worker (char-list)
	     (unless (endp char-list)
	       (let* ((char (car char-list))
		      (rest (cons char (worker (cdr char-list)))))
		 (if (find char "\\(),!&|<>=")
		     (cons #\\ rest)
		     rest)))))
    (coerce (worker (coerce string 'list))
	    'string)))
			   
(defun compose-ldap-search-arguments (filter attributes
				      &key
                                      bind-dn
				      base
				      (scope :sub)
;				      (password-file *password-file*)
                                      password-file
                                      password
				      (simple-auth-p *simple-auth-p*)
                                      starttlsp
                                      host)
  "Helper function to handle arglists common to searching functions."
  (format nil "~{~{ ~S~}~}"
	  (list (when bind-dn
                  (list "-D" bind-dn))
                (when password-file
		  (list "-y" (namestring (truename password-file))))
                (when password
                  (list "-w" password)); TODO: remove when finished
		(when base
		  (list "-b" base))
		(when simple-auth-p
		  (list "-x"))
		(when starttlsp
		  (list "-ZZ"))
                (when host
                  (list "-h" host))
		(list "-s" (ecase scope
			     (:sub "sub")
			     (:base "base")
			     (:one "one")))
		(list "-LLL")
		(list
		 (cond
		   ((zerop (length filter)) "(objectClass=*)")
		   ((stringp filter) filter)
		   ((listp filter) (convert-search-filter filter))))
		attributes)))

(defun ldap-search (&rest args)
  "Searches by FILTER and returns matching ATTRIBUTES.
FILTER may be a filter string in the form expected by ldap_search(3),
or a sexp which will be converted by CONVERT-SEARCH-FILTER.  A null
FILTER is equivalent to \"(objectClass=*)\".  ATTRIBUTES is a list of
strings, each string designating the name of an attribute.  If
ATTRIBUTES is empty, all attributes of a matching record will be
returned."
  (with-input-from-string (ldif (funcall #'trivial-shell:shell-command
                                         (concatenate 'string *ldapsearch-command*
                                                      (apply #'compose-ldap-search-arguments args))))
    (parse-ldif-stream ldif)))

(defun ldap-fetch-by-dn (dn attributes
			 &rest args)
  "Fetches the object designated by the distinguished name DN.
ATTRIBUTES are handled as by LDAP-SEARCH."
  (car (apply 'ldap-search "" attributes
	      :base dn
	      :scope :base
	      args)))

(defun compose-ldap-modify-arguments (&key
                                      bind-dn
				      ;(password-file *password-file*)
                                      password-file
                                      password
				      (simple-auth-p *simple-auth-p*)
                                      starttlsp
                                      host)
  "Helper function to handle arglists common to modifying functions."
  (format nil "~{~{ ~S~}~}"
	  (list (when bind-dn
                  (list "-D" bind-dn))
                (when password-file
		  (list "-y" *password-file*))
                (when password
                  (list "-w" password)); TODO: remove when finished
		(when simple-auth-p
		  (list "-x"))
                (when starttlsp
		  (list "-ZZ"))
                (when host
                  (list "-h" host)))))

(defun ldap-modify (change-records &rest args)
  "Transmits CHANGE-RECORDS to the LDAP server for execution."
  (with-open-stream (ldif (make-string-output-stream))
    (dolist (record change-records change-records)
      (write-change-record record ldif)
      (funcall #'trivial-shell:shell-command
               (concatenate 'string *ldapmodify-command*
                            (apply #'compose-ldap-modify-arguments args))
               :input (get-output-stream-string ldif)))))


(defun ldap-delete-by-dn (dn &rest args)
  "Delete record with dn"
  (funcall #'trivial-shell:shell-command
           (concatenate 'string *ldapdelete-command*
                        (apply #'compose-ldap-modify-arguments args)
                        " " dn)))

