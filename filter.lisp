(in-package :clldap)

(defun convert-search-filter (filter)
  "Returns a filter string created from the FILTER sexp.

Example:
  (convert-search-filter '(and (not (or (= \"objectClass\" \"computer\")
                                        (= \"objectClass\" \"group\")))
                               (= \"location\" \"millenia*\")))
  => \"(&(!(|(objectClass=computer)(objectClass=group)))(location=millenia*))\""
  (cond
    ((null filter) "")
    ((member (car filter) '(= ~= <= >=))
     (destructuring-bind (filter-type attribute-type attribute-value)
	 filter
       (format nil "(~A~A~A)"
	       (ldap-escape attribute-type) filter-type (ldap-escape attribute-value))))
    ((member (car filter) '(and or))
     (format nil "(~A~{~A~})"
	     (ecase (car filter)
	       (and #\&)
	       (or #\|))
	     (mapcar 'convert-search-filter (cdr filter))))
    ((eq (car filter) 'not)
     (format nil "(!~A)" (convert-search-filter (cadr filter))))))