(in-package :clldap)

(defvar *line-number*)

(defun continuation-line-p (line)
  (and (not (zerop (length line)))
       (eql (elt line 0) #\Space)))

(defun parse-ldif-line (line)
  (cond
    ((null line) nil)
    ((zerop (length line)) 'separator)
    ((eql (elt line 0) #\#) (values 'comment (string-left-trim " " (subseq line 1))))
    ((continuation-line-p line) (values 'continuation (subseq line 1))) ; should never happen
    (t
     (let ((colon-pos (position #\: line)))
       (cond ((null colon-pos)
              (error "Missing #\: in ~S." line))
             ((<= (length line) (1+ colon-pos))
              (let ((attribute-description (split-sequence #\; (subseq line 0 colon-pos))))
                (list (car attribute-description) (cdr attribute-description) "")))
             (t
              (let ((content (case (elt line (1+ colon-pos))
                               (#\: (base64-string-to-string (string-left-trim " " (subseq line (+ 2 colon-pos)))))
                               (#\< (error "URL attribute values not supported."))
                               (t (string-left-trim " " (subseq line (1+ colon-pos))))))
                    (attribute-description (split-sequence #\; (subseq line 0 colon-pos))))
                (list (car attribute-description) (cdr attribute-description) content))))))))

(defun read-cleansed-line (stream)
  (let ((line (read-line stream)))
    (when line
      (string-right-trim '(#\Return #\Newline) line))))

(defun separator-line-p (line)
  (zerop (length line)))

(defun read-raw-ldif (stream)
  (let (working-line
	results
	(line-number (if (boundp '*line-number*) *line-number* 1)))
    (macrolet ((current-results ()
		 `(if working-line
		      (cons (parse-ldif-line working-line) results)
		      results)))
      (do-reader (line (read-cleansed-line stream)
			    (or (nreverse (current-results))
				(error 'end-of-file
				       :stream stream)))
	(if (continuation-line-p line)
	    (if working-line
		(setf working-line (concatenate 'string working-line (subseq line 1)))
		(error "Attempt to continue at start of record on ~:[record ~;~]line ~D."
		       (boundp '*line-number*) line-number))
	    (if (separator-line-p line)
		(return-from read-raw-ldif (nreverse (current-results)))
		(setf results (current-results)
		      working-line line
		      line-number (1+ line-number))))))))

(defun line-name (line)
  (car line))

(defun line-options (line)
  (cadr line))

(defun line-value (line)
  (caddr line))

(defun raw-ldif->record (ldif)
  (let (distinguished-name
	object-classes
	attributes)
    (dolist (line ldif
	     (list distinguished-name (nreverse object-classes) (nreverse attributes)))
      (cond
	((eq line 'comment))
	((string-equal (car line) "dn")
	 (cond
	   ((line-options line)
	    (error "Options not allowed for distinguished name."))
	   (distinguished-name
	    (error "Distinguished name already declared."))
	   (t
	    (setf distinguished-name (line-value line)))))
	((string-equal (car line) "objectclass")
	 (if distinguished-name
	     (setf object-classes (cons (cdr line) object-classes))
	     (error "Distinguished name not declared.")))
	(t
	 (if distinguished-name
	     (let ((attribute-values (assoc (line-name line) attributes
                                            :test #'string-equal)))
               (if attribute-values
                   (setf (cdr attribute-values)
                         (append (cdr attribute-values)
                                 (list (cdr line))))
                   (push (list (car line) (cdr line)) attributes)))
	     (error "Distinguished name not declared.")))))))
  
(defun read-record (&optional (stream *standard-input*) (eof-error-p t) eof-value)
  "Reads a single LDIF data record from STREAM."
  (raw-ldif->record (handler-case
			(read-raw-ldif stream)
		      (end-of-file (e)
			(if eof-error-p
			    (error e)
			    (return-from read-record eof-value))))))

(defun parse-ldif-stream (stream)
  "Returns a list of data records read from STREAM up to end-of-file."
  (map-reader 'raw-ldif->record (read-raw-ldif stream)))

(defun parse-ldif-file (pathname)
  "Returns a list of data records read from the file designated by PATHNAME."
  (with-open-file (ldif pathname)
    (parse-ldif-stream ldif)))
