(defpackage :clldap
  (:nicknames :ldap)
  (:use :common-lisp :cl-base64 :split-sequence)
  (:export #:*password-file* #:*simple-auth-p* #:*ldapsearch-command* #:*ldapmodify-command*
	   #:convert-search-filter
           #:ldap-search #:ldap-fetch-by-dn
           #:ldap-delete-by-dn
	   #:ldap-modify
           #:record-distinguished-name #:record-object-classes #:record-attributes-list #:record-attributes
	   #:record-attribute #:record-attribute-value
	   #:attribute-options #:attribute-value
	   #:change-record-change-type #:record->add-record #:record->replace-record
	   #:read-record #:parse-ldif-stream #:parse-ldif-file
	   #:write-record #:write-change-record))

(in-package :clldap)
