;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(in-package :asdf)

(defsystem :clldap
  :name "LDAP Library"
  :author "<vsync@quadium.net>"
  :licence "GNU LGPL"

  :components
  ((:file "package")
   (:file "utils-ldif" :depends-on ("package"))
   (:file "convert" :depends-on ("utils-ldif"))
   (:file "read" :depends-on ("utils-ldif"))
   (:file "write" :depends-on ("utils-ldif"))
   (:file "globals" :depends-on ("package"))
   (:file "utils" :depends-on ("globals"))
   (:file "filter" :depends-on ("globals")))

  :depends-on (:cl-base64 :split-sequence :trivial-shell))
