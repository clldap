(in-package :clldap)

(defparameter *password-file* nil
  "A string designating file containing a password to be used for LDAP authentication.")

(defparameter *simple-auth-p* nil
  "Whether the LDAP utilities should use simple authentication.")

(defparameter *ldapsearch-command* "/usr/bin/ldapsearch"
  "The command to invoke the \"ldapsearch\" utility.")

(defparameter *ldapmodify-command* "/usr/bin/ldapmodify"
  "The command to invoke the \"ldapmodify\" utility.")

(defparameter *ldapdelete-command* "/usr/bin/ldapdelete"
  "The command to invoke the \"ldapdelete\" utility.")

