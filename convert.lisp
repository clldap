(in-package :clldap)

(defun record->add-record (record)
  "Returns a change record with the ADD change type designed to create RECORD."
  (destructuring-bind (distinguished-name object-classes attributes)
      record
    (list distinguished-name
	  'add
	  (fold-object-classes-and-attributes object-classes attributes))))

(defun record->replace-record (record)
  "Returns a change record with the MODIFY change type designed to replace all attributes in RECORD."
  (destructuring-bind (distinguished-name object-classes attributes)
      record
    (list distinguished-name
	  'modify
	  (mapcar (lambda (attribute)
		    (apply 'list
			   (car attribute) 'replace (cdr attribute)))
		  (fold-object-classes-and-attributes object-classes attributes)))))

