(in-package :clldap)

(defun write-attributes (attributes &optional (stream *standard-output*))
  (dolist (attribute attributes)
      (let ((attribute-name (car attribute)))
	(dolist (attribute-value (cdr attribute))
	  (format stream "~A: ~A~%" attribute-name (cadr attribute-value))))))
  
(defun write-record (record &optional (stream *standard-output*))
  "Writes the data record RECORD as LDIF to STREAM."
  (destructuring-bind (distinguished-name object-classes attributes)
      record
    (format stream "dn: ~A~%" distinguished-name)
    (format stream "~{objectClass: ~A~%~}" (mapcar 'cadr object-classes))
    (write-attributes attributes stream)
    (terpri stream)))

(defun fold-object-classes-and-attributes (object-classes attributes)
  (if object-classes
	      (cons (cons "objectClass" object-classes)
		    attributes)
	      attributes))

(defun write-change-record (record &optional (stream *standard-output*))
  "Writes the change record RECORD as LDIF to STREAM."
  (destructuring-bind (distinguished-name change-type attributes)
      record
    (format stream "dn: ~A~%" distinguished-name)
    (format stream "changetype: ~A~%" (ecase change-type
					((add delete modify)
					 (string-downcase (symbol-name change-type)))))
    (ecase change-type
      (add (write-attributes attributes stream))
      (delete (assert (null attributes)))
      (modify
       (dolist (attribute attributes)
         (destructuring-bind (attribute-name attribute-action &rest attribute-values)
             attribute
           (format stream "~A: ~A~%"
                   (ecase attribute-action
                     ((add replace delete)
                      (string-downcase (symbol-name attribute-action))))
                   attribute-name)
	   (dolist (attribute-value attribute-values)
	     (format stream "~A: ~A~%" attribute-name (cadr attribute-value))))
	 (format stream "-~%"))))
    (terpri stream)))
